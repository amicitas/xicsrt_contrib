"""
Version Description:
major.minor.revision

The major and minor numbers should match the version of xicsrt that is
compatible with the current xicsrt_contrib release.

revision:
  - Incremental changes not attached to major xicsrt release.
"""

__version__="0.7.0"
