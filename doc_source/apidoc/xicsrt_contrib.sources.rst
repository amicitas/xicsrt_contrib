xicsrt_contrib.sources
======================

.. automodule:: xicsrt_contrib.sources
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Built-in Source Objects
-----------------------

.. toctree::
   :maxdepth: 1

   xicsrt_contrib.sources.XicsrtPlasmaImas
   xicsrt_contrib.sources.XicsrtPlasmaVmec
   xicsrt_contrib.sources.XicsrtPlasmaVmecDatafile


