API Documentation
=================

.. automodule:: xicsrt_contrib
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:


packages
--------

.. toctree::
   :maxdepth: 3

   xicsrt_contrib.sources
   xicsrt_contrib.optics
   xicsrt_contrib.filters

